﻿Public Class BRSETTING 

    Private Sub BRSETTING_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FTPIPTXT.Text = My.Settings.FTPIP
        FTPPASSTXT.Text = My.Settings.FTPPASSWORD
        FTPUSERTXT.Text = My.Settings.FTPUSER
        DIRMOVETXT.Text = My.Settings.DIRMOVE
        DIRREADTXT.Text = My.Settings.DIRREAD
        IPDBTXT.Text = My.Settings.IPDB
        PASSDBTXT.Text = My.Settings.DBPASSWORD
        PORTDBTXT.Text = My.Settings.DBPORT
        USERDBTXT.Text = My.Settings.DBUSER
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        My.Settings.DIRREAD = DIRREADTXT.Text
    End Sub
End Class