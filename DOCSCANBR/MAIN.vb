﻿
Imports System.Net
Imports System.Net.FtpClient
Imports System.Threading
Imports Inobix.Windows.Components.IBscanner

Public Class MAIN
    Dim ibscanner As BarcodeScanner = New BarcodeScanner
    Dim FTPconnect As FTP
    Dim pathRead As String = My.Settings.DIRREAD
    ' Dim pathRead As String = "Z:\"

    Dim dtset As dtset = New dtset
    Dim row As DataRow
    Dim dt As DataTable

    Private Sub MAIN_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim db As CONNECTDB = CONNECTDB.NewConnection
        Dim sql As String = "SELECT * FROM masconnect WHERE typecon='c';"
        dt = db.GetTable(sql)
        db.Dispose()
        Dim FTPaddress As String = dt.Rows(0).Item("ip").ToString
        Dim FTPuser As String = dt.Rows(0).Item("username").ToString
        Dim FTPpass As String = dt.Rows(0).Item("password").ToString
        FTPconnect = New FTP(FTPaddress, FTPuser, FTPpass)
        FTPconnect.Connect()
    End Sub
    Public Sub readData()

    End Sub

    Public Function readFileInDirectory() As DataSet
        Dim check As Boolean = False
        Try
            For Each Dir As String In System.IO.Directory.GetFiles(pathRead)
                Dim fileinfo As New System.IO.FileInfo(Dir)
                Dim arr As Array = fileinfo.Name.ToString.Split(".")
                If arr(1).ToString.ToLower <> "tif" And arr(1).ToString.ToLower <> "jpeg" And arr(1).ToString.ToLower <> "jpg" And arr(1).ToString.ToLower <> ".gif" And arr(1).ToString.ToLower <> ".png" And arr(1).ToString.ToLower <> ".bmp" Then
                    getFtpUploadFileError(fileinfo.Name)
                    Exit For
                End If
                For i As Integer = 0 To dtset.Tables("DTSCAN").Rows.Count - 1

                    If dtset.Tables("DTSCAN").Rows(i)("FILENAME") = fileinfo.Name Then
                        check = True
                        i = dtset.Tables("DTSCAN").Rows.Count - 1
                    Else
                        check = False
                    End If
                Next
                Try
                    If check = False Then
                        row = dtset.Tables("DTSCAN").NewRow()
                        row.Item("FILENAME") = fileinfo.Name

                        row.Item("STATUS") = "1"
                        Exit Function
                    End If
                Catch ex As Exception
                    ' MsgBox(ex.ToString)
                End Try


            Next


        Catch ex As IndexOutOfRangeException
        Catch ex As ArgumentOutOfRangeException
        Catch ex As Exception
            ' MsgBox(ex.ToString)
        End Try

    End Function
    Public Sub checkDeleteFile()
        Dim checks As Boolean = True
        For j As Integer = 0 To dtset.Tables("DTSCAN").Rows.Count - 1
            For Each Dir As String In System.IO.Directory.GetFiles(pathRead)
                Dim fileinfo As New System.IO.FileInfo(Dir)

                Try
                    If dtset.Tables("DTSCAN").Rows(j)("FILENAME") = fileinfo.Name Then
                        checks = False
                        Exit For
                    Else
                        checks = True
                    End If
                Catch ex As IndexOutOfRangeException

                Catch ex As ArgumentOutOfRangeException

                End Try
            Next
            If checks = True Then
                dtset.Tables("DTSCAN").Delete("FILENAME = '" & dtset.Tables("DTSCAN").Rows(j)("FILENAME") & "'")
            End If
        Next
    End Sub
    Public Sub updateFileReadValue(filename As String)
        ibscanner.EnableMultiThreading = False
        ibscanner.ScanningMode = Enumerations.ScanningModes.Normal
        SimpleButton1.Text = ibscanner.LicenseStatus
        Try
            ibscanner.ReadFromFile(pathRead & "\" & filename)

        Catch ex As Exception
            Exit Sub
        End Try
        Dim value As String = ""
        Dim count As String = ""
        Dim barcode As BarcodeItem
        For Each barcode In ibscanner.BarcodesList.Values
            ' Get barcode value  
            Dim codeValue As String = barcode.Barcode
            ' Get barcode type  
            Dim codeType As String = barcode.CodeType.ToString()
            ' Get how many times scanner found this code in the scanned image  
            count = barcode.RepeatCounter
            If value.Length > 1 Then
                value = value & "," & codeValue
            Else
                value = codeValue
            End If
        Next
        If ibscanner.BarcodesList.Values.Count >= 1 Then
            dtset.Tables("DTSCAN").Update("FILENAME ='" & filename & "'", "STATUS", 2)
            dtset.Tables("DTSCAN").Update("FILENAME ='" & filename & "'", "READVALUE", value)
            dtset.Tables("DTSCAN").Update("FILENAME ='" & filename & "'", "COUNTREAD", count)
        Else
            dtset.Tables("DTSCAN").Update("FILENAME ='" & filename & "'", "STATUS", 4)
            dtset.Tables("DTSCAN").Update("FILENAME ='" & filename & "'", "READVALUE", "FAILED")
            dtset.Tables("DTSCAN").Update("FILENAME ='" & filename & "'", "COUNTREAD", count)
        End If

        ibscanner.BarcodesList.Clear()
    End Sub

    Public Sub getFtpUploadFileError(Filename As String)
        Dim checkfolder As Boolean
        Dim PathDirectory As String = dt.Rows(0).Item("pathfolder").ToString.Replace("/", "\")
        If FTPconnect.DirectoryExists(PathDirectory & "\FILEER\" & Date.Now.ToString("yyyyMMdd")) Then
        Else
            FTPconnect.CreateDirectory(PathDirectory & "\FILEER\" & Date.Now.ToString("yyyyMMdd"), False)
        End If
        Try
            FTPconnect.DeleteFile(PathDirectory & "\FILEER\" & Date.Now.ToString("yyyyMMdd") & "\" & Filename)

        Catch ex As Exception
            ' MsgBox(ex.ToString)

        End Try

        Dim DirList As FtpListItem() = FTPconnect.GetListing(PathDirectory & "\FILEER\" & Date.Now.ToString("yyyyMMdd") & "\", FtpListOption.AllFiles)


        For i = 0 To DirList.Length - 1
            If DirList(i).Name = Filename Then
                checkfolder = True
                Exit For
            Else
                checkfolder = False
            End If
        Next
        If checkfolder = True Then
            FTPconnect.DeleteFile(PathDirectory & "\FILEER\" & Date.Now.ToString("yyyyMMdd") & "\" & Filename)
        End If
        Try

            If System.IO.File.GetAttributes(pathRead & "\" & Filename) & FileAttribute.Directory Then

                FTPconnect.UploadFile(New WebClient, pathRead & "\" & Filename, PathDirectory & "\FILEER\" & Date.Now.ToString("yyyyMMdd") & "\" & Filename)

            End If


        Catch ex As Exception
            '  MsgBox(ex.ToString)

        End Try





        dtset.Tables("DTSCAN").Update("FILENAME = '" & Filename & "'", "STATUS", "5")

    End Sub

    Public Sub getFtpUploadFile(ByVal HN As String, Folder As String, Filename As String, ByVal RENAMEFILE As String, Page As String)


        Dim PathDirectory As String = dt.Rows(0).Item("pathfolder").ToString.Replace("/", "\")
        If FTPconnect.FileExists(PathDirectory & "\" & HN, FtpListOption.AllFiles) Then

        Else
            FTPconnect.CreateDirectory(PathDirectory & "\" & HN, False)

        End If
        Dim DirList As FtpListItem() = FTPconnect.GetListing(PathDirectory & "\" & HN, FtpListOption.AllFiles)
        Dim checkfolder As Boolean = False
        For i = 0 To DirList.Length - 1
            If DirList(i).Name = Folder Then
                checkfolder = True
                Exit For
            Else
                checkfolder = False
            End If

        Next
        If checkfolder = False Then
            FTPconnect.CreateDirectory(PathDirectory & "\" & HN & "\" & Folder, True)
            checkfolder = True
        End If
        If checkfolder = True Then


            Dim checkfilename As Boolean = False


            Dim DirList1 As FtpListItem() = FTPconnect.GetListing(PathDirectory & "\" & HN & "\" & Folder & "\", FtpListOption.AllFiles)



            For i = 0 To DirList1.Length - 1

                If DirList1(i).Name = RENAMEFILE & "-" & Page & ".TIF" Then
                    checkfilename = True
                    Page = Page & "-" & i.ToString
                    i = 0
                Else
                    checkfilename = False
                End If
            Next

            FTPconnect.UploadFile(New WebClient, pathRead & "\" & Filename, PathDirectory & "\" & HN & "\" & Folder & "\" & RENAMEFILE & "-" & Page & ".TIF")



        End If

        dtset.Tables("DTSCAN").Update("FILENAME = '" & Filename & "'", "STATUS", "5")



    End Sub
    Public Sub deleteFile(pathfile As String)
        Try
            '  Kill(pathfile)
            System.GC.Collect()
            System.GC.WaitForPendingFinalizers()
            '  System.IO.File.SetAttributes(pathfile, FileAttribute.Normal)
            System.IO.File.Delete(pathfile)

        Catch ex As Exception
            ' MsgBox(ex.ToString)
        End Try


    End Sub


    Public Sub getAfterMoveFinish()
        For i As Integer = 0 To dtset.Tables("DTSCAN").Rows.Count - 1
            Try

                If dtset.Tables("DTSCAN").Rows(i)("STATUS") = 5 Then
                    deleteFile(pathRead & "\" & dtset.Tables("DTSCAN").Rows(i)("FILENAME"))
                End If
            Catch ex As Exception
                '   MsgBox(ex.ToString)
            End Try

        Next
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If bgStart.IsBusy = False Then
            Timer1.Stop()
            bgStart.RunWorkerAsync()

        End If

    End Sub
    Private Sub bgStart_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgStart.DoWork
        readFileInDirectory()

    End Sub

    Private Sub bgStart_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgStart.RunWorkerCompleted
        If row Is Nothing Then
        Else
            dtset.Tables("DTSCAN").Rows.Add(row)

        End If
        row = Nothing

        For i As Integer = 0 To dtset.Tables("DTSCAN").Rows.Count - 1
            If dtset.Tables("DTSCAN").Rows(i)("STATUS") = 1 Then

                updateFileReadValue(dtset.Tables("DTSCAN").Rows(i)("FILENAME"))
            End If
        Next
        Dim docmanage As New docMClass
        Application.DoEvents()

        For i As Integer = 0 To dtset.Tables("DTSCAN").Rows.Count - 1
            Try
                If dtset.Tables("DTSCAN").Rows(i)("STATUS") = 2 And dtset.Tables("DTSCAN").Rows(i)("READVALUE") <> "FAILED" And dtset.Tables("DTSCAN").Rows(i)("READVALUE").ToString.Length > 1 Then
                    Dim folder As String
                    Dim DOCNO As String
                    Dim page As String
                    Dim HNFOLDER As String = "FAILED"
                    Dim DOCTYPE As String
                    Try
                        DOCTYPE = Mid(dtset.Tables("DTSCAN").Rows(i)("READVALUE"), 1, 2)

                        If DOCTYPE = "DO" Then

                            DOCNO = Mid(dtset.Tables("DTSCAN").Rows(i)("READVALUE"), 3, 10)
                            HNFOLDER = docmanage.getAN(DOCNO)


                            If HNFOLDER = "FAILED" Then
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "HN", HNFOLDER)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "DOCNO", DOCNO)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "FOLDERTYPE", DOCTYPE)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "PAGE", 1)

                                dtset.Tables("DTSCAN").Update("FILENAME ='" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "STATUS", 4)
                            Else
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "HN", HNFOLDER)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "DOCNO", DOCNO)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "FOLDERTYPE", DOCTYPE)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "PAGE", 1)
                            End If



                        Else
                            DOCNO = Mid(dtset.Tables("DTSCAN").Rows(i)("READVALUE"), 3, 10)
                            Try
                                HNFOLDER = docmanage.GETHN(DOCNO)

                            Catch ex As Exception

                            End Try
                            folder = Mid(dtset.Tables("DTSCAN").Rows(i)("READVALUE"), 1, 2)
                            page = Mid(dtset.Tables("DTSCAN").Rows(i)("READVALUE"), 13, dtset.Tables("DTSCAN").Rows(i)("READVALUE").ToString.Length)
                            If HNFOLDER = "FAILED" Then
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "HN", HNFOLDER)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "DOCNO", DOCNO)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "FOLDERTYPE", folder)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "PAGE", page)

                                dtset.Tables("DTSCAN").Update("FILENAME ='" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "STATUS", 4)

                            Else
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "HN", HNFOLDER)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "DOCNO", DOCNO)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "FOLDERTYPE", folder)
                                dtset.Tables("DTSCAN").Update("FILENAME = '" & dtset.Tables("DTSCAN").Rows(i)("FILENAME") & "'", "PAGE", page)

                            End If
                        End If

                    Catch ex As Exception
                        ' MsgBox(ex.ToString)
                    End Try
                End If
            Catch ex As Exception
                ' MsgBox(ex.ToString)

            End Try

        Next

        Application.DoEvents()
        For i As Integer = 0 To dtset.Tables("DTSCAN").Rows.Count - 1
            If dtset.Tables("DTSCAN").Rows(i)("STATUS").ToString = 2 And dtset.Tables("DTSCAN").Rows(i)("HN").ToString <> "" And dtset.Tables("DTSCAN").Rows(i)("HN").ToString <> "FAILED" Then


                getFtpUploadFile(dtset.Tables("DTSCAN").Rows(i)("HN").ToString, dtset.Tables("DTSCAN").Rows(i)("FOLDERTYPE").ToString, dtset.Tables("DTSCAN").Rows(i)("FIlENAME").ToString, dtset.Tables("DTSCAN").Rows(i)("DOCNO").ToString, dtset.Tables("DTSCAN").Rows(i)("PAGE").ToString)
            ElseIf dtset.Tables("DTSCAN").Rows(i)("HN").ToString = "FAILED" Or dtset.Tables("DTSCAN").Rows(i)("READVALUE").ToString = "FAILED" Then
                getFtpUploadFileError(dtset.Tables("DTSCAN").Rows(i)("FIlENAME").ToString)


            End If



        Next
        getAfterMoveFinish()
        checkDeleteFile()
        dgvReavalue.DataSource = dtset
        dgvReavalue.ForceInitialize()




        Timer1.Start()
    End Sub
    Public Shared Function KillSleepingConnections() As Integer
        Dim strSQL As String = "show processlist"
        Dim m_ProcessesToKill As System.Collections.ArrayList = New ArrayList()

        Dim connecdb As CONNECTDB = CONNECTDB.NewConnection
        Dim dt As New DataTable
        dt = connecdb.GetTable(strSQL)


        Try
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim iPID As Integer = Convert.ToInt32(dt.Rows(i)("Id").ToString())
                Dim iTime As Integer = Convert.ToInt32(dt.Rows(i)("Time").ToString())
                Dim strState As String = dt.Rows(i)("Command").ToString()

                If strState = "Sleep" AndAlso iPID > 0 And iTime > 45 Then
                    ' This connection is sitting around doing nothing. Kill it.
                    m_ProcessesToKill.Add(iPID)
                End If
            Next
            ' Get a list of processes to kill.


            For Each aPID As Integer In m_ProcessesToKill
                strSQL = "kill " + aPID.ToString
                connecdb.ExecuteNonQuery(strSQL)
            Next
        Catch excep As Exception
        Finally
            connecdb.Dispose()
        End Try

        Return m_ProcessesToKill.Count
    End Function

    ' Call it with the following code to kill sleeping connections >= 100 seconds.

    '=======================================================
    'Service provided by Telerik (www.telerik.com)
    'Conversion powered by NRefactory.
    'Twitter: @telerik
    'Facebook: facebook.com/telerik
    '=======================================================

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Dim nextform As New BRSETTING
        nextform.Show()

    End Sub
End Class
