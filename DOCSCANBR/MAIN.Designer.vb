﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MAIN
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgvReavalue = New DevExpress.XtraGrid.GridControl()
        Me.Dtset1 = New DOCSCANBR.dtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colFIlENAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTATUS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREADVALUE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDOCNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFOLDERTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPAGE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOUNTREAD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bgStart = New System.ComponentModel.BackgroundWorker()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.dgvReavalue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvReavalue
        '
        Me.dgvReavalue.Cursor = System.Windows.Forms.Cursors.Default
        Me.dgvReavalue.DataMember = "DTSCAN"
        Me.dgvReavalue.DataSource = Me.Dtset1
        Me.dgvReavalue.Location = New System.Drawing.Point(12, 12)
        Me.dgvReavalue.MainView = Me.GridView1
        Me.dgvReavalue.Name = "dgvReavalue"
        Me.dgvReavalue.Size = New System.Drawing.Size(1001, 298)
        Me.dgvReavalue.TabIndex = 0
        Me.dgvReavalue.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Dtset1
        '
        Me.Dtset1.DataSetName = "dtset"
        Me.Dtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFIlENAME, Me.colSTATUS, Me.colREADVALUE, Me.colDOCNO, Me.colHN, Me.colFOLDERTYPE, Me.colPAGE, Me.colCOUNTREAD})
        Me.GridView1.GridControl = Me.dgvReavalue
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colFIlENAME
        '
        Me.colFIlENAME.FieldName = "FIlENAME"
        Me.colFIlENAME.Name = "colFIlENAME"
        Me.colFIlENAME.Visible = True
        Me.colFIlENAME.VisibleIndex = 0
        '
        'colSTATUS
        '
        Me.colSTATUS.FieldName = "STATUS"
        Me.colSTATUS.Name = "colSTATUS"
        Me.colSTATUS.Visible = True
        Me.colSTATUS.VisibleIndex = 1
        '
        'colREADVALUE
        '
        Me.colREADVALUE.FieldName = "READVALUE"
        Me.colREADVALUE.Name = "colREADVALUE"
        Me.colREADVALUE.Visible = True
        Me.colREADVALUE.VisibleIndex = 2
        '
        'colDOCNO
        '
        Me.colDOCNO.FieldName = "DOCNO"
        Me.colDOCNO.Name = "colDOCNO"
        Me.colDOCNO.Visible = True
        Me.colDOCNO.VisibleIndex = 3
        '
        'colHN
        '
        Me.colHN.FieldName = "HN"
        Me.colHN.Name = "colHN"
        Me.colHN.Visible = True
        Me.colHN.VisibleIndex = 4
        '
        'colFOLDERTYPE
        '
        Me.colFOLDERTYPE.FieldName = "FOLDERTYPE"
        Me.colFOLDERTYPE.Name = "colFOLDERTYPE"
        Me.colFOLDERTYPE.Visible = True
        Me.colFOLDERTYPE.VisibleIndex = 5
        '
        'colPAGE
        '
        Me.colPAGE.FieldName = "PAGE"
        Me.colPAGE.Name = "colPAGE"
        Me.colPAGE.Visible = True
        Me.colPAGE.VisibleIndex = 6
        '
        'colCOUNTREAD
        '
        Me.colCOUNTREAD.FieldName = "COUNTREAD"
        Me.colCOUNTREAD.Name = "colCOUNTREAD"
        Me.colCOUNTREAD.Visible = True
        Me.colCOUNTREAD.VisibleIndex = 7
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'bgStart
        '
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(480, 317)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(528, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Edit"
        '
        'MAIN
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1025, 354)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.dgvReavalue)
        Me.Name = "MAIN"
        Me.Text = "Form1"
        CType(Me.dgvReavalue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvReavalue As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Dtset1 As DOCSCANBR.dtset
    Friend WithEvents colFIlENAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTATUS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREADVALUE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDOCNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFOLDERTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPAGE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOUNTREAD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents bgStart As System.ComponentModel.BackgroundWorker
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton

End Class
