﻿Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Globalization
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Drawing
Imports System.Windows.Forms

Imports System.Data
Module DTMODULE

    <System.Runtime.CompilerServices.Extension> _
    Public Function Delete(table As DataTable, filter As String) As DataTable
        table.[Select](filter).Delete()
        table.AcceptChanges()
        Return table
    End Function
    <System.Runtime.CompilerServices.Extension> _
    Public Sub Delete(rows As IEnumerable(Of DataRow))
        For Each row As DataRow In rows
            row.Delete()
        Next
    End Sub
    <System.Runtime.CompilerServices.Extension> _
    Public Function Update(table As DataTable, filter As String, colname As String, value As String) As DataTable
        table.[Select](filter).Update(colname, value)
        Return table
    End Function
    <System.Runtime.CompilerServices.Extension> _
    Public Sub Update(rows As IEnumerable(Of DataRow), col As String, value As String)
        For Each row As DataRow In rows
            row(col) = value
        Next
    End Sub

End Module
